use serde::Deserialize;

#[derive(Debug, Deserialize)]
#[serde(rename_all = "camelCase")]
pub struct EngineStatus {
    pub time_stamp: i64,
    pub engines: Vec<IvrEngine>,
}

#[derive(Debug, Deserialize)]
#[serde(rename_all = "camelCase")]
pub struct IvrEngine {
    // pub date_time: String,
    pub active_count: i32,
    pub visible: bool,
    pub channels: Vec<IvrChannel>,
    pub active_percent: i32,
    pub engine_name: String,
}

#[derive(Debug, Deserialize)]
#[serde(rename_all = "camelCase")]
pub struct IvrChannel {
    pub visible: bool,
    pub active: bool,
    pub id: i32,
    pub status: String,

    pub phone: Option<String>,
    pub utility: Option<String>,
}
