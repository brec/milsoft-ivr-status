use serde::Serialize;

#[derive(Serialize)]
#[serde(rename_all = "camelCase")]
pub struct Login {
    pub last_time: usize,
    pub user: String,
    pub password: String,
}

impl Login {
    pub fn new_login(user: &str, password: &str) -> Login {
        Login {
            last_time: 0,
            user: user.to_string(),
            password: password.to_string(),
        }
    }
}
