use crate::{
    error::IvrError,
    model::{engine::EngineStatus, post::Login},
};
use error_stack::{Result, ResultExt};
use reqwest::Response;

pub async fn get_engine_status(login: &Login, url: &str) -> Result<EngineStatus, IvrError> {
    let response = get_engine_status_response(login, url).await?;
    response_to_json(response).await
}

pub async fn get_engine_status_raw(login: &Login, url: &str) -> Result<String, IvrError> {
    let response = get_engine_status_response(login, url).await?;
    response_to_text(response).await
}

async fn get_engine_status_response(login: &Login, url: &str) -> Result<Response, IvrError> {
    reqwest::Client::new()
        .post(url)
        .json(&login)
        .send()
        .await
        .change_context_lazy(|| IvrError::WebAccess)
}

async fn response_to_json(response: Response) -> Result<EngineStatus, IvrError> {
    response
        .json()
        .await
        .change_context_lazy(|| IvrError::JsonDeserialization)
}

async fn response_to_text(response: Response) -> Result<String, IvrError> {
    response
        .text()
        .await
        .change_context_lazy(|| IvrError::WebAccess)
}
