// #![allow(unused)]

use clap::Parser;
use env_logger::{Builder, Env};
use log::Level;
use milsoft_ivr_status::{db::SqlConnection, error::IvrError, prelude::EmptyResult};
use std::env;

const POSTGRES_CONN_STRING: &str = "POSTGRES_CONN_STRING";

#[tokio::main]
async fn main() -> EmptyResult<IvrError> {
    dotenv::dotenv().ok();
    Builder::from_env(Env::default().default_filter_or(Level::Warn.as_str())).init();
    let opts = Options::parse();

    let conn_string = env::var(POSTGRES_CONN_STRING).unwrap();
    let connection = SqlConnection::new_connection(&conn_string).await?;
    if opts.from_zero {
        connection.clear_states().await?;
    }
    connection.process_states().await
}

#[derive(Parser)]
struct Options {
    #[clap(short = 'z', long = "from-zero", action)]
    /// Clear existing records and start over processing.
    from_zero: bool,
}
