// #![allow(unused)]

use clap::Parser;
use env_logger::{Builder, Env};
use log::{debug, trace, Level};
use milsoft_ivr_status::{
    db::SqlConnection, error::IvrError, model::post::Login, prelude::EmptyResult,
    web::get_engine_status_raw,
};
use std::env;

const POSTGRES_CONN_STRING: &str = "POSTGRES_CONN_STRING";

#[tokio::main]
async fn main() -> EmptyResult<IvrError> {
    dotenv::dotenv().ok();
    Builder::from_env(Env::default().default_filter_or(Level::Warn.as_str())).init();
    let opts = Options::parse();

    let login = Login::new_login(&opts.username, &opts.password);
    let url = build_url(&opts);
    debug!("REST url: {url}");

    let json_string = get_engine_status_raw(&login, &url).await?;
    trace!("{json_string}");

    let conn_string = env::var(POSTGRES_CONN_STRING).unwrap();
    let connection = SqlConnection::new_connection(&conn_string).await?;
    let inserted = connection.post_string(&json_string).await?;
    debug!("Inserted {inserted} rows");

    Ok(())
}

fn build_url(opts: &Options) -> String {
    let proto = match opts.bls_ssl {
        true => "https",
        false => "http",
    };
    let port = match (opts.bls_ssl, opts.bls_port) {
        (true, 443) => String::new(),
        (false, 80) => String::new(),
        (_, port) => format!(":{port}"),
    };
    format!("{}://{}{}{}", proto, opts.bls_host, port, opts.bls_endpoint)
}

#[derive(Parser)]
struct Options {
    #[clap(short = 's', long = "ssl", env = "MILSOFT_IVR_BLS_SSL", action)]
    /// BLS SSL enabled (true/false)
    bls_ssl: bool,

    #[clap(
        short = 'b',
        long = "bls",
        env = "MILSOFT_IVR_BLS_HOST",
        default_value = "127.0.0.1"
    )]
    /// BLS host
    bls_host: String,

    #[clap(
        short = 'o',
        long = "port",
        env = "MILSOFT_IVR_BLS_PORT",
        default_value_t = 8080
    )]
    /// BLS host port
    bls_port: u16,

    #[clap(
        long = "endpoint",
        env = "MILSOFT_IVR_BLS_ENDPOINT",
        default_value = "/ivr/rest/core/getEngineStatus"
    )]
    /// BLS endpoint
    bls_endpoint: String,

    #[clap(short = 'u', long = "username", env = "MILSOFT_IVR_BLS_USERNAME")]
    /// BLS username
    username: String,

    #[clap(short = 'p', long = "password", env = "MILSOFT_IVR_BLS_PASSWORD")]
    /// BLS password
    password: String,
}
