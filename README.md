# Milsoft IVR Status Tools

Collection of tools for monitoring the status of a Milsoft IVR system.

## Build

The tools in this package are built as examples both to separate the exe only dependencies and simplify the Cargo.toml file.

Make sure to build with `--examples`, i.e. `cargo build --release --examples`.

## Package

This project includes a WiX configuration to build a windows installer package with the WiX tool. The cargo-wix tool should build this properly with `cargo wix`.

### Note

Installing cargo-wix with `cargo install cargo-wix` should work if your rust install is otherwise operational, and does not require the WiX toolkit to be available for it to succeed.

Unhelpfully, the documentation for cargo-wix isn't particularly clear on the fact that it does not support versions of WiX newer than v3, and the WiX installer page will direct you to use `dotnet` to install the current version of the tool which is v5 at the this time. You will instead need to get a v3 WiX toolkit (https://github.com/wixtoolset/wix3/releases) for this to work.

## Environment

These tools can use environment variables to configure themselves at runtime. Some options can also be passed on the command line if this is not a suitable choice for your environment. All tools will helpfully (I hope, at least) emit useful information about the supported options with `--help`. 

There are a few options not revealed in the `--help` output.
* `POSTGRES_CONN_STRING` - You must provide a postgres connection string. More information can be found here: https://docs.rs/tokio-postgres/latest/tokio_postgres/config/struct.Config.html
* `RUST_LOG` - An `env_logger` string here will change the logging level for internal components of these tools. Notable loggers:
  * `reqwest` - Status information for the REST calls made
  * `tokio_postgres` - Status information for database interactions

## Tools

### `ivr-update`

Pulls the channel status of all engines on the specified server, and inserts the JSON state snapshot emitted into a postgres database for later evaluation.

### `ivr-channel-decode`

Builds metadata from stored JSON to describe saved state snapshots.
