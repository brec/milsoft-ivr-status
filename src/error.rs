use std::fmt::Display;

use error_stack::Context;

#[derive(Debug)]
pub enum IvrError {
    JsonDeserialization,
    SqlConfiguration,
    SqlInsert,
    SqlReset,
    SqlQuery,
    WebAccess,
}

impl Context for IvrError {}
impl Display for IvrError {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        let message = match self {
            Self::JsonDeserialization => "Error deserializing JSON",
            Self::SqlConfiguration => "Error with database settings",
            Self::SqlInsert => "Error inserting state",
            Self::SqlReset => "Error while resetting state log",
            Self::SqlQuery => "Error with SQL query",
            Self::WebAccess => "Error pulling from REST endpoint",
        };

        write!(f, "{}", message)
    }
}
