pub type EmptyResult<T> = error_stack::Result<(), T>;
