use chrono::DateTime;
use error_stack::{Result, ResultExt};
use log::{debug, error, trace};
use tokio_postgres::{types::ToSql, Client, NoTls};

use crate::{error::IvrError, model::engine::EngineStatus, prelude::EmptyResult};

pub struct SqlConnection {
    client: Client,
}

impl SqlConnection {
    pub async fn new_connection(conn_string: &str) -> Result<Self, IvrError> {
        let (client, connection) = tokio_postgres::connect(conn_string, NoTls)
            .await
            .change_context_lazy(|| IvrError::SqlConfiguration)?;

        tokio::spawn(async move {
            if let Err(e) = connection.await {
                error!("{:?}", e);
            } else {
                debug!("Connected");
            }
        });

        Ok(SqlConnection { client })
    }

    pub async fn clear_states(&self) -> EmptyResult<IvrError> {
        let statement = "TRUNCATE channel_status RESTART IDENTITY CASCADE";
        self.client
            .execute(statement, &[])
            .await
            .change_context_lazy(|| IvrError::SqlReset)
            .map(|_| ())
    }

    pub async fn process_states(&self) -> Result<(), IvrError> {
        let statement = "SELECT * FROM unprocessed_states";
        let rows = self
            .client
            .query(statement, &[])
            .await
            .change_context_lazy(|| IvrError::SqlQuery)?;

        for row in rows {
            let row_id: i64 = row.get(0);
            let state: &str = row.get(1);
            let status: EngineStatus = serde_json::from_str(state)
                .change_context_lazy(|| IvrError::JsonDeserialization)?;
            let row_time = DateTime::from_timestamp_millis(status.time_stamp).unwrap();

            let insert_statement = "INSERT INTO channel_status (e_text_id, e_time, e_channel, e_in_use, e_status) VALUES ($1, $2, $3, $4, $5)";
            // Assume single engine
            if let Some(engine) = status.engines.first() {
                for channel in engine.channels.iter() {
                    let channel_id = channel.id;
                    let in_use = channel.active;
                    let channel_status = &channel.status;
                    // debug!("{}", status.engines.first().unwrap().date_time);
                    trace!("{row_id}, {row_time}, {channel_id}, {in_use}, {channel_status}");
                    self.client
                        .execute(
                            insert_statement,
                            &[&row_id, &row_time, &channel_id, &in_use, channel_status],
                        )
                        .await
                        .change_context_lazy(|| IvrError::SqlInsert)?;
                }
                debug!("Inserted state for: {row_time}");
            }
        }

        Ok(())
    }

    pub async fn post_string<T>(&self, json_string: &T) -> Result<u64, IvrError>
    where
        T: ToSql + Sync,
    {
        let statement =
            "INSERT INTO channel_status_text (e_time, e_status) VALUES (LOCALTIMESTAMP, $1::text)";
        let inserted = self
            .client
            .execute(statement, &[json_string])
            .await
            .change_context_lazy(|| IvrError::SqlQuery)?;

        Ok(inserted)
    }
}
